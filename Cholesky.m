function R = Cholesky(A)
%Cholesky factorization based on Algorithim 23.1. A is the incoming matrix,
%R is the resultant.

R = A;
%find dimentions of A
[m,n] = size(A);

%Report errors for incompatible A
if m ~= n
    error('Not a square matrix')
end

if ~isequal(A,A')
    error('Not a Hermitian Matrix')
end


%Preform Algorithm.
for k = 1:m
    for j = k+1 : m
        R(j,j:m) = R(j,j:m) - R(k,j:m)*R(k,j)'/R(k,k);
    end
    R(k,k:m) = R(k,k:m)/sqrt(R(k,k));
end

%removes lower tirangular values (error in algorithim?)
for k = 2:m
    for j = 1:k-1
        R(k,j) = 0;
    end
end
