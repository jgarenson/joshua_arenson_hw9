function TimeTest(n)
%Creates  a plot of runtime comparison between LU decom and Cholesky decomp
%up to nxn matrix

%preallocation
LUtime = zeros(n,1);
Choltime = zeros(n,1);

%gets runtime for 1-n nxn matricies
for k = 1:n
    %generate matrix
    A = RandSym(k);
    
    %LU decomp runtime
    tic 
    [L ,U] = GE_PartialPivot(A);
    LUtime(k) = toc;
    
    %Cholesky Decomp
    tic
    R = Cholesky(A);
    Choltime(k) = toc;
end

figure
hold on 

%figure labels
title('Comparison of LU and Cholesky runtime.')
xlabel('n')
ylabel('Run Time')

plot(1:n,LUtime,'-b.',1:n,Choltime,'-r.')
legend('LU Decomp','Cholesky Decomp')
