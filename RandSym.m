function A = RandSym(n)
%Generates an random hermatian nxn matrix with element values bewteen 0 and
%1. 

%generate random nxn matrix with values 0->1
A = rand(n);

%make systemtric by multiplie A by complex transpose.
A = A*A';